<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>updateBooking</name>
   <tag></tag>
   <elementGuidId>62455544-c0c9-49a1-bff4-ef5fbb4aca13</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>${GlobalVariable.token}</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;firstname\&quot; : \&quot;${firstname}\&quot;,\n    \&quot;lastname\&quot; : \&quot;${lastname}\&quot;,\n    \&quot;totalprice\&quot; : \&quot;${totalprice}\&quot;,\n    \&quot;depositpaid\&quot; : \&quot;${depositpaid}\&quot;,\n    \&quot;bookingdates\&quot; : {\n        \&quot;checkin\&quot; : \&quot;${checkin}\&quot;,\n        \&quot;checkout\&quot; : \&quot;${checkout}\&quot;\n    },\n    \&quot;additionalneeds\&quot; : \&quot;${additionalneeds}\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>d9f615a0-448a-418b-a6db-1129e0c68943</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>4a2ffb6a-caf5-4e9d-8df3-7e1311b60ff0</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Cookie</name>
      <type>Main</type>
      <value>token = ${GlobalVariable.token}</value>
      <webElementGuid>55a8a0ca-995b-4c64-8680-319e37128a3a</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${GlobalVariable.urlWeb}/booking/${GlobalVariable.bookingID}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'Aditya Tri'</defaultValue>
      <description></description>
      <id>bd147457-818b-43fa-8836-89fe22c14dad</id>
      <masked>false</masked>
      <name>firstname</name>
   </variables>
   <variables>
      <defaultValue>'Herdiansyah'</defaultValue>
      <description></description>
      <id>0087d645-be2b-420b-bfaf-84210c0a2576</id>
      <masked>false</masked>
      <name>lastname</name>
   </variables>
   <variables>
      <defaultValue>200</defaultValue>
      <description></description>
      <id>b0f02c31-daf0-4b7a-bb4d-919cdc784db6</id>
      <masked>false</masked>
      <name>totalprice</name>
   </variables>
   <variables>
      <defaultValue>true</defaultValue>
      <description></description>
      <id>56557efa-cadd-4403-bf2b-abcea2ad1726</id>
      <masked>false</masked>
      <name>depositpaid</name>
   </variables>
   <variables>
      <defaultValue>'2023-07-15'</defaultValue>
      <description></description>
      <id>6fd85dd1-967e-44d4-9718-23b82d6e4026</id>
      <masked>false</masked>
      <name>checkin</name>
   </variables>
   <variables>
      <defaultValue>'2023-07-28'</defaultValue>
      <description></description>
      <id>bbbc5196-c52f-4b98-b500-0c532a0b0bea</id>
      <masked>false</masked>
      <name>checkout</name>
   </variables>
   <variables>
      <defaultValue>'Dinner'</defaultValue>
      <description></description>
      <id>9e7955cc-92d5-4457-9377-00d16ccb3c8f</id>
      <masked>false</masked>
      <name>additionalneeds</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

def variables = request.getVariables()
def firstname = variables.get('firstname')
def lastname = variables.get('lastname')
def totalprice = variables.get('totalprice')
def depositpaid = variables.get('depositpaid')
def checkin = variables.get('checkin')
def checkout = variables.get('checkout')
def additionalneeds = variables.get('additionalneeds')

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyElementPropertyValue(response, 'firstname', firstname)
WS.verifyElementPropertyValue(response, 'lastname', lastname)
WS.verifyElementPropertyValue(response, 'totalprice', totalprice)
WS.verifyElementPropertyValue(response, 'depositpaid', depositpaid)
WS.verifyElementPropertyValue(response, 'bookingdates.checkin', checkin)
WS.verifyElementPropertyValue(response, 'bookingdates.checkout', checkout)
WS.verifyElementPropertyValue(response, 'additionalneeds', additionalneeds)
assertThat(response.getStatusCode()).isEqualTo(200)

GlobalVariable.firstname = WS.getElementPropertyValue(response, 'firstname')
GlobalVariable.lastname = WS.getElementPropertyValue(response, 'lastname')
GlobalVariable.totalprice = WS.getElementPropertyValue(response, 'totalprice')
GlobalVariable.depositpaid = WS.getElementPropertyValue(response, 'depositpaid')
GlobalVariable.checkin = WS.getElementPropertyValue(response, 'bookingdates.checkin')
GlobalVariable.checkout = WS.getElementPropertyValue(response, 'bookingdates.checkout')
GlobalVariable.additionalneeds = WS.getElementPropertyValue(response, 'additionalneeds')</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
