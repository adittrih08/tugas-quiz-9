<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>createBooking</name>
   <tag></tag>
   <elementGuidId>38c7760d-871b-456e-9d20-1e1ece4b9b9d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>${GlobalVariable.token}</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;firstname\&quot; : \&quot;${firstname}\&quot;,\n    \&quot;lastname\&quot; : \&quot;${lastname}\&quot;,\n    \&quot;totalprice\&quot; : \&quot;${totalprice}\&quot;,\n    \&quot;depositpaid\&quot; : \&quot;${depositpaid}\&quot;,\n    \&quot;bookingdates\&quot; : {\n        \&quot;checkin\&quot; : \&quot;${checkin}\&quot;,\n        \&quot;checkout\&quot; : \&quot;${checkout}\&quot;\n    },\n    \&quot;additionalneeds\&quot; : \&quot;${additionalneeds}\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>d9f615a0-448a-418b-a6db-1129e0c68943</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>4a2ffb6a-caf5-4e9d-8df3-7e1311b60ff0</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${GlobalVariable.token}</value>
      <webElementGuid>55a8a0ca-995b-4c64-8680-319e37128a3a</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.urlWeb}/booking</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'Aditya'</defaultValue>
      <description></description>
      <id>378af33e-3f39-44f2-a9a5-1418da567865</id>
      <masked>false</masked>
      <name>firstname</name>
   </variables>
   <variables>
      <defaultValue>'Tri'</defaultValue>
      <description></description>
      <id>ebbf0e57-1770-4719-a8d6-7a73515f177c</id>
      <masked>false</masked>
      <name>lastname</name>
   </variables>
   <variables>
      <defaultValue>141</defaultValue>
      <description></description>
      <id>94ae9345-05fc-43a7-87e1-a823bde43246</id>
      <masked>false</masked>
      <name>totalprice</name>
   </variables>
   <variables>
      <defaultValue>true</defaultValue>
      <description></description>
      <id>eec1b174-074d-4606-a7c1-3a804291bd4b</id>
      <masked>false</masked>
      <name>depositpaid</name>
   </variables>
   <variables>
      <defaultValue>'2023-08-20'</defaultValue>
      <description></description>
      <id>d45538ab-d3e6-42d5-9eb1-62f07663e621</id>
      <masked>false</masked>
      <name>checkin</name>
   </variables>
   <variables>
      <defaultValue>'2023-08-24'</defaultValue>
      <description></description>
      <id>2f1c9e14-359f-40f3-98df-ce43442d0816</id>
      <masked>false</masked>
      <name>checkout</name>
   </variables>
   <variables>
      <defaultValue>'Breakfast'</defaultValue>
      <description></description>
      <id>c40629a9-47fa-4135-bb80-bd8e1e78c6a1</id>
      <masked>false</masked>
      <name>additionalneeds</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

def variables = request.getVariables()
def firstname = variables.get('firstname')
def lastname = variables.get('lastname')
def totalprice = variables.get('totalprice')
def depositpaid = variables.get('depositpaid')
def checkin = variables.get('checkin')
def checkout = variables.get('checkout')
def additionalneeds = variables.get('additionalneeds')

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyElementPropertyValue(response, 'booking.firstname', firstname)
WS.verifyElementPropertyValue(response, 'booking.lastname', lastname)
WS.verifyElementPropertyValue(response, 'booking.totalprice', totalprice)
WS.verifyElementPropertyValue(response, 'booking.depositpaid', depositpaid)
WS.verifyElementPropertyValue(response, 'booking.bookingdates.checkin', checkin)
WS.verifyElementPropertyValue(response, 'booking.bookingdates.checkout', checkout)
WS.verifyElementPropertyValue(response, 'booking.additionalneeds', additionalneeds)
assertThat(response.getStatusCode()).isEqualTo(200)

GlobalVariable.bookingID = WS.getElementPropertyValue(response, 'bookingid')
GlobalVariable.firstname = WS.getElementPropertyValue(response, 'booking.firstname')
GlobalVariable.lastname = WS.getElementPropertyValue(response, 'booking.lastname')
GlobalVariable.totalprice = WS.getElementPropertyValue(response, 'booking.totalprice')
GlobalVariable.depositpaid = WS.getElementPropertyValue(response, 'booking.depositpaid')
GlobalVariable.checkin = WS.getElementPropertyValue(response, 'booking.bookingdates.checkin')
GlobalVariable.checkout = WS.getElementPropertyValue(response, 'booking.bookingdates.checkout')
GlobalVariable.additionalneeds = WS.getElementPropertyValue(response, 'booking.additionalneeds')

System.out.println(GlobalVariable.bookingID)
System.out.println(GlobalVariable.firstname)
System.out.println(GlobalVariable.lastname)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
